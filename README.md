# Java Object Flattener

just a utility function that returns the object and its descendants as a flat list.

it allows you to make any arbitrary object tree easily iterable.

what it actually does is best explained by the tests i think.

```java
class FlattenTest {
    @Test
    void iterableFlatteningOn() {
        var whereOne = new WhereClause("one");
        var whereTwo = new WhereClause("two");
        var whereClauses = List.of(whereOne, whereTwo);
        var fromClause = new FromClause("from", whereClauses);
        
        var expected = List.of(fromClause, "from", String.CASE_INSENSITIVE_ORDER,
                whereOne, "one", String.CASE_INSENSITIVE_ORDER,
                whereTwo, "two", String.CASE_INSENSITIVE_ORDER);
        
        assertEquals(expected, Flatten.flattenObject(fromClause, true));
    }

    @Test
    void hiddenPropertyPositive() {
        var hidden = new Hidden(true);
        var hider = new Hider(hidden);
        
        var expect = List.of(hider, hidden);
        
        assertEquals(expect, Flatten.flattenObject(hider, true, true));
    }

    public static class Hider {
        private final Hidden hidden;

        public Hider(Hidden hidden) {
            this.hidden = hidden;
        }
    }

    public static class Hidden {
        public boolean lol;

        public Hidden(boolean lol) {
            this.lol = lol;
        }
    }
}
```

## Installation
### Maven
```xml
<dependencies>
    <dependency>
        <groupId>cc.rbbl</groupId>
        <artifactId>java-object-flattener</artifactId>
        <version>1.0.4</version>
    </dependency>
</dependencies>
```

### Gradle kts
```kotlin
dependencies {
    implementation("cc.rbbl:java-object-flattener:1.0.4")
}
```