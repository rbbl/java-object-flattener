package cc.rbbl.jof;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Flatten {

    /**
     * allows you to make any arbitrary object tree easy iterable.
     *
     * @param input given input Object
     * @return a {@link List} of the given input Object and its descendant (children, grandchildren..) Properties.
     */
    public static List<Object> flattenObject(Object input) {
        return flattenObject(input, new ArrayList<>(), false, false);
    }

    /**
     * allows you to make any arbitrary object tree easy iterable.
     *
     * @param input            given input Object
     * @param flattenIterables when true it adds all {@link Iterable} items to the returned list instead of the iterable itself
     * @return a {@link List} of the given input Object and its descendant (children, grandchildren..) Properties.
     */
    public static List<Object> flattenObject(Object input, boolean flattenIterables) {
        return flattenObject(input, new ArrayList<>(), flattenIterables, false);
    }

    /**
     * allows you to make any arbitrary object tree easy iterable.
     *
     * @param input                      given input Object
     * @param flattenIterables           when true it adds all {@link Iterable} items to the returned list instead of the iterable itself
     * @param includeInvisibleProperties set to true to include all properties and not only public ones
     * @return a {@link List} of the given input Object and its descendant (children, grandchildren..) Properties.
     */
    public static List<Object> flattenObject(Object input, boolean flattenIterables, boolean includeInvisibleProperties) {
        return flattenObject(input, new ArrayList<>(), flattenIterables, includeInvisibleProperties);
    }

    private static List<Object> flattenObject(Object input, List<Object> existingItems, boolean flattenIterable, boolean includeInvisibleProperties) {
        List<Object> result = new ArrayList<>();
        if (input == null || existingItems.contains(input) || input instanceof Enum<?>) {
            return result;
        }
        if (flattenIterable && input instanceof Iterable) {
            for (Object o : (Iterable<?>) input) {
                result.addAll(flattenObject(o, result, flattenIterable, includeInvisibleProperties));
            }
        } else {
            result.add(input);
            var newObjects = getPropertyValues(input, includeInvisibleProperties);
            for (Object object : newObjects) {
                result.addAll(flattenObject(object, result, flattenIterable, includeInvisibleProperties));
            }
        }
        return result;
    }

    private static List<Object> getPropertyValues(Object input, boolean includeInvisibleProperties) {
        var stream = Arrays.stream(input.getClass().getDeclaredFields());
        if (includeInvisibleProperties) {
            stream = stream.peek(field -> field.setAccessible(true));
        } else {
            stream = stream.filter(field -> Modifier.isPublic(field.getModifiers()));
        }

        return stream.filter(field -> !field.getType().isPrimitive()).map(field -> {
            try {
                return field.get(input);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());
    }
}

