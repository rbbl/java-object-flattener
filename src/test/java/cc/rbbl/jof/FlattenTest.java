package cc.rbbl.jof;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FlattenTest {

    @Test
    void flattenObject() {
        var from = new FromClause("from");
        var select = new Select("select", from);
        var view = new SqlView(1, "view", select);

        var expected = List.of(view, "view", String.CASE_INSENSITIVE_ORDER,
                select, "select", String.CASE_INSENSITIVE_ORDER,
                from, "from", String.CASE_INSENSITIVE_ORDER);

        assertEquals(expected, Flatten.flattenObject(view));
    }

    @Test
    void flattenObject2() {
        var expected = List.of("view", String.CASE_INSENSITIVE_ORDER);

        assertEquals(expected, Flatten.flattenObject("view"));
    }

    @Test
    void flattenObjectNull() {
        var expected = List.of();

        assertEquals(expected, Flatten.flattenObject(null));
    }

    @Test
    void flattenObjectNullNested() {
        var from = new FromClause(null);
        var select = new Select("select", from);
        var view = new SqlView(1, "view", select);

        var expected = List.of(view, "view", String.CASE_INSENSITIVE_ORDER, select, "select", String.CASE_INSENSITIVE_ORDER, from);

        assertEquals(expected, Flatten.flattenObject(view));
    }

    @Test
    void selfReferenceTest() {
        var testee = new Nestable();
        testee.nested = testee;

        var expected = List.of(testee);

        assertEquals(expected, Flatten.flattenObject(testee));
    }

    @Test
    void iterableFlatteningOff() {
        var whereClauses = List.of(new WhereClause("one"), new WhereClause("two"));
        var fromClause = new FromClause("from", whereClauses);

        var expected = List.of(fromClause, "from", String.CASE_INSENSITIVE_ORDER, whereClauses);

        assertEquals(expected, Flatten.flattenObject(fromClause));
    }

    @Test
    void iterableFlatteningOn() {
        var whereOne = new WhereClause("one");
        var whereTwo = new WhereClause("two");
        var whereClauses = List.of(whereOne, whereTwo);
        var fromClause = new FromClause("from", whereClauses);

        var expected = List.of(fromClause, "from", String.CASE_INSENSITIVE_ORDER,
                whereOne, "one", String.CASE_INSENSITIVE_ORDER,
                whereTwo, "two", String.CASE_INSENSITIVE_ORDER);

        assertEquals(expected, Flatten.flattenObject(fromClause, true));
    }

    @Test
    void iterableInputFlatteningOff() {
        var whereOne = new WhereClause("one");
        var whereTwo = new WhereClause("two");
        var whereClauses = List.of(whereOne, whereTwo);

        var expected = List.of(whereClauses);

        assertEquals(expected, Flatten.flattenObject(whereClauses, false));
    }

    @Test
    void iterableInputFlatteningOn() {
        var whereOne = new WhereClause("one");
        var whereTwo = new WhereClause("two");
        var whereClauses = List.of(whereOne, whereTwo);

        var expected = List.of(whereOne, "one", String.CASE_INSENSITIVE_ORDER,
                whereTwo, "two", String.CASE_INSENSITIVE_ORDER);

        assertEquals(expected, Flatten.flattenObject(whereClauses, true));
    }

    @Test
    void nestedIterableInputFlatteningOff() {
        var whereOne = new WhereClause("one");
        var whereTwo = new WhereClause("two");
        var whereClauses = List.of(List.of(whereOne, whereTwo));

        var expected = List.of(whereClauses);

        assertEquals(expected, Flatten.flattenObject(whereClauses, false));
    }

    @Test
    void nestedIterableInputFlatteningOn() {
        var whereOne = new WhereClause("one");
        var whereTwo = new WhereClause("two");
        var whereClauses = List.of(List.of(whereOne, whereTwo));

        var expected = List.of(whereOne, "one", String.CASE_INSENSITIVE_ORDER,
                whereTwo, "two", String.CASE_INSENSITIVE_ORDER);

        assertEquals(expected, Flatten.flattenObject(whereClauses, true));
    }

    @Test
    void hiddenPropertyPositive() {
        var hidden = new Hidden(true);
        var hider = new Hider(hidden);

        var expect = List.of(hider, hidden);

        assertEquals(expect, Flatten.flattenObject(hider, true, true));
    }

    @Test
    void hiddenPropertyNegative() {
        var hidden = new Hidden(true);
        var hider = new Hider(hidden);

        var expect = List.of(hider);

        assertEquals(expect, Flatten.flattenObject(hider, true, false));
    }

    @Test
    void testWithEnumInTree() {
        var where = new WhereClause(Idk.One);

        var expect = List.of(where);

        assertEquals(expect, Flatten.flattenObject(where));
    }

    @Test
    void testWithEnumAsInput() {
        var enm = Idk.One;

        var expect = List.of();

        assertEquals(expect, Flatten.flattenObject(enm));
    }

    public static class SqlView {
        public int test;
        public String sql;
        public Select select;

        SqlView(int test, String sql, Select select) {
            this.test = test;
            this.sql = sql;
            this.select = select;
        }
    }

    public static class Select {
        public String sql;
        public FromClause clause;

        Select(String sql, FromClause clause) {
            this.sql = sql;
            this.clause = clause;
        }
    }

    public static class FromClause {
        public String sql;
        public List<WhereClause> clauses;

        FromClause(String sql) {
            this.sql = sql;
        }

        FromClause(String sql, List<WhereClause> clauses) {
            this.sql = sql;
            this.clauses = clauses;
        }
    }

    public static class WhereClause {
        public String sql;
        public Idk idk;

        public WhereClause(String sql) {
            this.sql = sql;
        }

        public WhereClause(Idk idk) {
            this.idk = idk;
        }
    }

    public enum Idk {
        One, Two
    }

    public static class Hider {
        private final Hidden hidden;

        public Hider(Hidden hidden) {
            this.hidden = hidden;
        }
    }

    public static class Hidden {
        public boolean lol;

        public Hidden(boolean lol) {
            this.lol = lol;
        }
    }

    public static class Nestable {
        public Nestable nested;
    }
}