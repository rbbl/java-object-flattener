plugins {
    id("java")
    id("maven-publish")
    id("signing")
}

group = "cc.rbbl"
version = System.getenv("BUILD_VERSION") ?: "0.0.1-SNAPSHOT"

repositories {
    mavenCentral()
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
    withJavadocJar()
    withSourcesJar()
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.1")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}


publishing {
    publications.create<MavenPublication>("Lib") {
        from(components["java"])
        pom {
            name.set("Java Object Flattener")
            description.set("just a utility function that returns the object and its descendants as a flat list")
            url.set("https://www.rbbl.cc/")
            licenses {
                license {
                    name.set("MIT License")
                    url.set("https://gitlab.com/rbbl/java-object-flattener/-/blob/master/LICENSE")
                }
            }
            developers {
                developer {
                    id.set("rbbl-dev")
                    name.set("rbbl-dev")
                    email.set("dev@rbbl.cc")
                }
            }
            scm {
                url.set("https://gitlab.com/rbbl/java-object-flattener")
            }
        }
    }
    repositories {
        maven {
            name = "Snapshot"
            url = uri("https://s01.oss.sonatype.org/content/repositories/snapshots/")
            credentials(PasswordCredentials::class)
        }
        maven {
            name = "Central"
            url = uri("https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/")
            credentials(PasswordCredentials::class)
        }
    }
}

signing {
    val signingKey: String? by project
    val signingPassword: String? by project
    useInMemoryPgpKeys(signingKey, signingPassword)
    setRequired({
        gradle.taskGraph.allTasks.filter { it.name.endsWith("ToCentralRepository") }.isNotEmpty()
    })
    sign(publishing.publications)
}
